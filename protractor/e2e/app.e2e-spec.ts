import { TourOfHeroesPage } from './app.po';

describe('Buscar héroes', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToDashboard();
  });

  it('Buscar héroes', () => {
    page.enterHeroSearch('as');
    const count = page.countSearchResult().count();
    expect(2).toBe(count.then((n) => n));
  });

});

describe('Eliminar un heroe', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateTo();
  });

  it('Eliminar un heroe', () => {
    page.navigateToHeroes();
    const currentHeroes = page.getAllHeroes().count();
    page.deleteFirstHero();
    expect(page.getAllHeroes().count().then(n => n)).toBe(currentHeroes.then(n => n - 1));
  });

});

describe('Editar un héroe', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateTo();
  });

  it('Editar un héroe', () => {
    page.detailHeroWithName('Mr. Nice');
    page.changeHeroName(' edited');
    page.waitForElementVisible('.module.hero');
    const hero = page.getHeroWithName('M');
    expect(hero.getText().then(n => n)).toBe('Mr. Nice edited');
  });

});

describe('Navegar a un héroe desde el dashboard', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateTo();
  });

  it('Navegar a un héroe desde el dashboard', () => {
    page.detailHeroWithName('Mr. Nice');
    const value = page.getInputHeroNameValue();
    expect(value.then(n => n)).toBe('Mr. Nice');
  });

});

describe('Navegar a un héroe desde la lista de héroes', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateTo();
  });

  it('Navegar a un héroe desde la lista de héroes', () => {
    page.navigateToHeroes();
    page.detailHeroListWithName('11');
    const value = page.getInputHeroNameValue();
    expect(value.then(n => n)).toBe('Mr. Nice');
  });

});

describe('Navegar a un héroe desde la búsqueda', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToDashboard();
  });

  it('Navegar a un héroe desde la búsqueda', () => {
    page.enterHeroSearch('Mr');
    page.detailHeroSearchWithName('Mr');
    const value = page.getInputHeroNameValue();
    expect(value.then(n => n)).toBe('Mr. Nice');
  });

});
