import { browser, by, element, ElementFinder, protractor } from 'protractor';
export class TourOfHeroesPage {

  navigateTo() {

    return browser.get('/');
  }
  navigateToDashboard() {
    return browser.get('/dashboard');
  }

  getTop4Heroes() {
    return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
  }

  navigateToHeroes() {
    element(by.linkText('Heroes')).click();
  }

  getAllHeroes() {
    return element(by.tagName('my-heroes')).all(by.tagName('li'));
  }

  enterNewHeroInInput(newHero: string) {
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Add')).click();
  }

  enterHeroSearch(keys: string) {
    element(by.id('search-box')).sendKeys(keys);
  }

  countSearchResult() {
    return element.all(by.className('search-result'));
  }

  deleteFirstHero() {
    element(by.css('.heroes')).all(by.className('delete')).first().click();
  }

  detailHeroWithName(keyword: string) {
    element(by.cssContainingText('.module.hero', keyword)).click();
  }
  detailHeroListWithName(keyword: string) {
    element(by.cssContainingText('.badge', keyword)).click();
    element(by.cssContainingText('button', 'View Details')).click();

  }

  changeHeroName(keyword: string) {
    element(by.tagName('input')).sendKeys(keyword);
    element(by.cssContainingText('button', 'Save')).click();
  }

  getInputHeroNameValue() {
    return element(by.tagName('input')).getAttribute('ng-reflect-model');
  }

  getHeroWithName(keyword: string) {
    return element(by.cssContainingText('.module.hero', keyword));
  }

  waitForElementVisible(css: string) {
    const EC = protractor.ExpectedConditions;
    const elm = element(by.css(css));
    browser.wait(EC.presenceOf(elm), 5000);
  }

  detailHeroSearchWithName(keyword: string) {
    return element(by.cssContainingText('.search-result', keyword)).click();
  }

}
