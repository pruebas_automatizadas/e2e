describe('Los estudiantes under monkeys', function () {
    it('visits los estudiantes and survives monkeys', function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
        //randomClick(10);
        randomEvent(10);
    })
})
function randomClick(monkeysLeft) {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };

    var monkeysLeft = monkeysLeft;
    if (monkeysLeft > 0) {
        cy.get('a').then($links => {
            var randomLink = $links.get(getRandomInt(0, $links.length));
            if (!Cypress.dom.isHidden(randomLink)) {
                cy.wrap(randomLink).click({ force: true });
                monkeysLeft = monkeysLeft - 1;
            }
            setTimeout(randomClick, 1000, monkeysLeft);
        });
    }
}


function randomEvent(eventsCount) {
    console.log("Event number ", eventsCount);

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    };

    function getRandomEvent() {
        let event = "TEXT";
        let number = Math.floor(Math.random() * (4 - 1)) + 1;
        if (number === 1) {
            event = "CLICK-BUTTON";
        } else if (number === 2) {
            event = "CLICK-LINK";
        } else if (number === 3) {
            event = "SELECT";
        }
        return event;
    };

    var eventsCount = eventsCount;
    if (eventsCount > 0) {
        let event = getRandomEvent();
        console.log("Event generated ", event);
        if (event === "CLICK-BUTTON") {
            cy.get('button').then($inputs => {
                console.log("Found Buttons", $inputs.length);
                var randomInput = $inputs.get(getRandomInt(0, $inputs.length));
                if (!Cypress.dom.isHidden(randomInput)) {
                    cy.wrap(randomInput).click({ force: true });
                    eventsCount = eventsCount - 1;
                    console.log("Click event done");
                }
                setTimeout(randomEvent(eventsCount), 1000);
            });
        } else if (event === "CLICK-LINK") {
            cy.get('a').then($links => {
                console.log("Found Links", $links.length);
                var randomLink = $links.get(getRandomInt(0, $links.length));
                if (!Cypress.dom.isHidden(randomLink)) {
                    cy.wrap(randomLink).click({ force: true });
                    eventsCount = eventsCount - 1;
                    console.log("Click event done");
                }
                setTimeout(randomEvent(eventsCount), 1000);
            });
        } else if (event === "TEXT") {
            cy.get('input[type="text"]').then($inputs => {
                console.log("Found Inputs", $inputs.length);
                var randomInput = $inputs.get(getRandomInt(0, $inputs.length));
                cy.wrap(randomInput).type("123abc", { force: true });
                eventsCount = eventsCount - 1;
                console.log("Test event done");
                setTimeout(randomEvent(eventsCount), 1000);
            });
        } else if (event === "SELECT") {
            cy.get('select').then($selects => {
                console.log("Found selects", $selects.length);
                var randomInput = $selects.get(getRandomInt(0, $selects.length));
                var option = randomInput.options[getRandomInt(0, randomInput.options.length)];
                cy.wrap(randomInput).select(option.value, { force: true });
                console.log("option", option);
                eventsCount = eventsCount - 1;
                console.log("Test event done");
                setTimeout(randomEvent(eventsCount), 1000);
            });
        }
    }
}