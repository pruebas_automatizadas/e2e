var assert = require('assert');


describe('Login los estudiantes ', function () {

    it('Deberia loguear exitosamente', function () {

        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.pause(500);
        browser.waitForVisible('button=Ingresar', 20000);
        browser.waitForEnabled('button=Ingresar', 20000);
        browser.scroll('button=Ingresar');
        browser.click('button=Ingresar');

        browser.waitForVisible('input[name="correo"]', 20000);
        var cajaLogIn = browser.element('.cajaLogIn');

        browser.pause(500);
        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('shd_cristo@hotmail.com');

        browser.pause(500);
        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('ServioPantoja@1');

        cajaLogIn.element('.logInButton').click();
        browser.waitForVisible('#cuenta', 20000);
        isExisting = browser.isExisting('#cuenta');
        expect(isExisting).toBe(true);
    });
});