var assert = require('assert');


describe('Login los estudiantes ', function () {
    it('Deberia notificar error de cuenta ya existente', function () {

        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.pause(500);
        browser.waitForVisible('button=Ingresar', 20000);
        browser.waitForEnabled('button=Ingresar', 20000);
        browser.scroll('button=Ingresar');
        browser.click('button=Ingresar');

        browser.waitForVisible('input[name="nombre"]', 20000);
        var cajaSignUp = browser.element('.cajaSignUp');

        var nombreInput = cajaSignUp.element('input[name="nombre"]');
        nombreInput.click();
        nombreInput.keys('Servio');

        var apellidoInput = cajaSignUp.element('input[name="apellido"]');
        apellidoInput.click();
        apellidoInput.keys('Pantoja');

        browser.pause(500);
        var emailInput = cajaSignUp.element('input[name="correo"]');
        emailInput.click();
        emailInput.keys('shd_cristo@hotmail.com');

        browser.pause(500);
        var selectUniversidad = cajaSignUp.element('select[name="idUniversidad"]');
        selectUniversidad.selectByVisibleText("Universidad de los Andes");

        browser.pause(500);
        var selectPrograma = cajaSignUp.element('select[name="idPrograma"]');
        selectPrograma.selectByVisibleText("Administración");

        browser.pause(500);
        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('ServioPantoja');

        var checkAceptar = cajaSignUp.element('input[name="acepta"]');
        checkAceptar.click();

        var buttonAceptar = cajaSignUp.element('.logInButton');
        buttonAceptar.click();

        browser.waitForVisible('.sweet-alert', 20000);
        var alertText = browser.element('.sweet-alert h2').getText();
        expect(alertText).toBe('Ocurrió un error activando tu cuenta');

    });

});