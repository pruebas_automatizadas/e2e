module.exports = {
    'Busqueda de profesores': function (browser) {
        browser
            .maximizeWindow()
            .url('https://losestudiantes.co/')
            .click('.botonCerrar')
            .pause(1000)
            .click('.Select-control')
            .setValue('.Select-input input', 'Mario Linares Vasquez')
            .waitForElementVisible('.Select-menu-outer .Select-option', 5000)
            .assert.containsText('.Select-menu-outer .Select-option', 'Mario Linares Vasquez - Ingeniería De Sistemas')
            .end();
    },
    'Dirigirse a la página de un profesor': function (browser) {
        browser
            .maximizeWindow()
            .url('https://losestudiantes.co/')
            .click('.botonCerrar')
            .pause(1000)
            .click('.Select-control')
            .setValue('.Select-input input', 'Mario Linares Vasquez')
            .waitForElementVisible('.Select-menu-outer .Select-option', 5000)
            .click('.Select-menu-outer .Select-option')
            .waitForElementVisible('.nombreProfesor', 5000)
            .assert.containsText('.nombreProfesor', 'Mario Linares Vasquez')
            .end();
    },
    'Filtros por materia en la página de un profesor': function (browser) {
        browser
            .maximizeWindow()
            .url('https://losestudiantes.co/')
            .click('.botonCerrar')
            .pause(1000)
            .click('.Select-control')
            .setValue('.Select-input input', 'Mario Linares Vasquez')
            .waitForElementVisible('.Select-menu-outer .Select-option', 5000)
            .click('.Select-menu-outer .Select-option')
            .waitForElementVisible('.nombreProfesor', 5000)
            .click('.materias input')
            .pause(2000)
            .assert.containsText('.post .carreraCalificacion', 'Constr. Aplicaciones Móviles')
            .end();
    }
};